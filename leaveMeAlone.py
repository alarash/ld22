import pygame
from pygame.locals import *
from base import *

from menuScene import MenuScene
#===============================================================================
# 
#===============================================================================
if __name__ == "__main__":

    Game(size = (320, 240), #(640, 480),
         background = (240, 240, 240), #(255, 255, 255), #(170, 210, 210), 
         zoom = 2,
         debug = True)

    Game.SetScene(MenuScene)
    Game.Run()
