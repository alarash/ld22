import math

import pygame
from pygame.locals import *
from base import *

from playScene import *
#===============================================================================
# 
#===============================================================================
class MenuScene(Scene):
    def create(self):
        #super(MenuScene, self).__init__()
        self.bg = Sprite(img = "title.png")

        self.txt = Text(txt = "Press X to Start", fontSize = 14)
        self.txt.centerX = 160

        self.nodes.append(self.bg)
        self.nodes.append(self.txt)

        self.counter = 0

    def update(self, delta):
        if Game.keys[K_x]:
            Game.SetScene(PlayScene)
        elif Game.keys[K_ESCAPE]:
            Game.inst.quit = True

        self.counter += 6 * delta
        self.txt.y = 70 - 20 * abs(math.sin(self.counter))
        self.txt.scale = Vector(self.txt.w * math.sin(self.counter * 0.3), self.txt.h)
