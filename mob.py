import pygame
from pygame.locals import *
from base import *

from ground import *

#===============================================================================
# 
#===============================================================================
class Mob(AnimSprite):
    def __init__(self, x = 0, y = 0):
        super(Mob, self).__init__(x, y)

    def update(self, delta):
        self.bottom = Ground.GetHeight(self.x + self.w * 0.5)
        super(Mob, self).update(delta)

#===============================================================================
# 
#===============================================================================
class Photo(Mob):
    def __init__(self, x = 0, y = 0):
        super(Photo, self).__init__(x, y)
        self.size.set(48, 48)
        self.setImage("photo01.png")

        self.addAnim("run", (0, 1), 100)


class Photo2(Mob):
    def __init__(self, x = 0, y = 0, anim = "run"):
        super(Photo2, self).__init__(x, y)
        self.size.set(24, 24)
        self.setImage("photo01.png")

        self.addAnim("run", (10, 11), 100)
        self.addAnim("fan", (15, 16), 100)
        self.play(anim)

class Photo3(Mob):
    def __init__(self, x = 0, y = 0):
        super(Photo3, self).__init__(x, y)
        self.size.set(72, 90)
        #self.setImage("photo01.png")
        self.image = Data.GetSubImg("photo01.png", 48, 48, 72, 72)
