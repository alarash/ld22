import pygame
from pygame.locals import *
from base import *

from ground import *

class BG01(AnimSprite):
    def __init__(self, x = 0, y = 0):
        super(BG01, self).__init__(x, y, 24, 96, img = "bg01.png")
        self.images = self.tilesheet.getImgs()
        self.image = random.choice(self.images)

        self.bottom = Ground.GetHeight(self.x + 12) + 20

    def update(self, delta):
        self.vel.x = -Ground.inst.speed
        self.bottom = Ground.GetHeight(self.x + 12) + 20

        super(BG01, self).update(delta)


class Cloud(AnimSprite):
    def __init__(self, x = 0, y = 0):
        super(Cloud, self).__init__(x, y, 44, 32, img = "clouds.png")
        self.images = self.tilesheet.getImgs()
        self.image = random.choice(self.images)

    def update(self, delta):
        self.vel.x = -20
        super(Cloud, self).update(delta)
