from node import *

#===============================================================================
# Scene
#===============================================================================
class Scene(Node):
    scenes = {}

    @classmethod
    def Inst(cls):
        if not cls.scenes.has_key(cls):
            cls.scenes[cls] = cls()
        return cls.scenes[cls]

    def __init__(self):
        super(Scene, self).__init__()
        self.created = False

        self.nodes = []

    def preCreate(self):
        if self.created: return
        self.create()
        self.created = True

    def create(self):
        pass

    def update(self, delta):
        for node in self.nodes:
            node.update(delta)

    def draw(self, surface, offset = (0, 0)):
        for node in self.nodes:
            node.draw(surface, offset)
