import pygame
from pygame.locals import *

from node import *
from data import *

#===============================================================================
# Sprite
#===============================================================================
class Sprite(Node, pygame.sprite.Sprite):
    def __init__(self, x = 0, y = 0, w = 0, h = 0, img = None):
        Node.__init__(self, x, y, w, h)
        pygame.sprite.Sprite.__init__(self)

        self.__scale = Vector(1, 1)
        self.scaleOffset = Vector(0, 0)

        self.rect = pygame.rect.Rect(x, y, w, h)

        self.refImage = None
        self.image = None
        if img != None:
            self.setImage(img)

    def __repr__(self):
        return Node.__repr__(self)

    def setImage(self, img):
        self.refImage = Data.GetImg(img)
        self.image = Data.GetImg(img)

    def addAnim(self, name, indexes, frame = 20):
        self.anims[name] = (indexes, frame)
        if len(self.anims) == 1:
            self.play(name)

    def play(self, name):
        self.animId = name
        self.imgId = 0

    def update(self, delta):
        super(Sprite, self).update(delta)

    def draw(self, surface, offset = (0, 0)):
        if self.image == None: return
        if self.scale != self.__scale:
            self.calcScale()

        self.rect.x = self.x + offset[0] + self.scaleOffset.x
        self.rect.y = self.y + offset[1] + self.scaleOffset.y
        surface.blit(self.image, self.rect)

    #===========================================================================
    # Scale
    #===========================================================================
    def get_scale(self):
        return self.__scale
    def set_scale(self, val):
        self.__scale = val
        self.__calcScale()
    scale = property(get_scale, set_scale)

    def __calcScale(self):
        self.image = pygame.transform.scale(self.refImage, (abs(int(self.scale.x)), abs(int(self.scale.y))))
        self.image = pygame.transform.flip(self.image, self.scale.x < 0, self.scale.y < 0)

        self.rect.w = abs(self.scale.x)
        self.rect.h = abs(self.scale.y)

        self.scaleOffset.set(self.w * 0.5 - abs(self.scale.x * 0.5), self.h * 0.5 - abs(self.scale.y * 0.5))



#===============================================================================
# 
#===============================================================================
class AnimSprite(Sprite):
    def __init__(self, x = 0, y = 0, w = 0, h = 0, img = None):
        super(AnimSprite, self).__init__(x, y, w, h, None)

        self.imgId = 0
        self.animId = None
        self.anims = {}
        self.lastUpdate = 0

        self.tilesheet = None

        self.images = []
        self.image = None
        if img != None:
            self.setImage(img)

    def setImage(self, img, w = None, h = None):
        if w == None: w = self.w
        if h == None: h = self.h

        self.tilesheet = Data.GetTileSheet(img, w, h)
        #self.images = self.tilesheet.getImgs()#Data.GetTileSheet(img, w, h)
        #self.image = self.images[0]
        #self.image = self.tilesheet.image

    def addAnim(self, name, indexes, frame = 20):
        inds = []
        for index in indexes:
            img = self.tilesheet.getImg(index)

            if img in self.images:
                ind = self.images.index(img)
            else:
                self.images.append(img)
                ind = len(self.images) - 1

            inds.append(ind)

        self.anims[name] = (inds, frame)
        if len(self.anims) == 1:
            self.play(name)

    def play(self, name):
        self.animId = name
        self.imgId = 0

    def update(self, delta):
        super(Sprite, self).update(delta)
        self.updateImage(delta)


    def updateImage(self, delta):
        tick = pygame.time.get_ticks()

        if len(self.images) == 0: return
        if self.animId == None: return

        imgs = self.anims[self.animId][0]
        frame = self.anims[self.animId][1]

        if tick - self.lastUpdate >= frame:
            self.imgId += 1
            self.lastUpdate = tick

        if self.imgId >= len(imgs):
            self.imgId = 0

        self.image = self.images[imgs[self.imgId]]


#===============================================================================
# Text Label
#===============================================================================
class Text(Sprite):
    def __init__(self, x = 0, y = 0, txt = " ", font = None, fontSize = 10, color = (0, 0, 0)):
        super(Text, self).__init__(x = x, y = y)

        self.__fontSize = fontSize
        self.__text = txt
        self.__color = color
        #self.__needCalcRender = True
        self.__calcRender()

    #===========================================================================
    # Text
    #===========================================================================
    def get_text(self):
        return self.__text

    def set_text(self, val):
        self.__text = val
        #self.__needCalcRender = True
        self.__calcRender()

    text = property(get_text, set_text)
    #===========================================================================
    # Size
    #===========================================================================
    def get_fontSize(self):
        return self.__size

    def set_fontSize(self, val):
        self.__fontSize = val
        self.__needCalcRender = True

    fontSize = property(get_fontSize, set_fontSize)

    #===========================================================================
    # Color
    #===========================================================================
    def get_color(self):
        return self.__color

    def set_color(self, val):
        self.__color = val
        self.__needCalcRender = True

    color = property(get_color, set_color)
    #===========================================================================
    # 
    #===========================================================================
    def __calcRender(self):
        font = pygame.font.Font(Data.GetFont(), self.__fontSize)
        self.image = font.render(self.__text, 1, self.__color)
        rect = self.image.get_rect()
        self.size.set(rect.w, rect.h)

        self.refImage = self.image

        self.__needCalcRender = False

    #===========================================================================
    # 
    #===========================================================================
    def draw(self, surface, offset = (0, 0)):
        if self.__needCalcRender:
            self.__calcRender()

        super(Text, self).draw(surface, offset)
