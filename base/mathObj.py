#===============================================================================
# Vector
#===============================================================================
class Vector(object):
    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y

    def set(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, v):
        return Vector(self.x + v.x, self.y + v.y)

    def __iadd__(self, v):
        self.x += v.x
        self.y += v.y
        return self

    def __mul__(self, val):
        if isinstance(val, (int, float)):
            return Vector(self.x * val, self.y * val)

    def __eq__(self, v):
        return self.x == v.x and self.y == v.y

    def __ne__(self, v):
        return not self.__eq__(v)

    def __repr__(self):
        return "%s(%s, %s)" % (self.__class__.__name__, self.x, self.y)

#===============================================================================
# 
#===============================================================================
class Rect(object):
    def __init__(self, x = 0, y = 0, w = 0, h = 0):
        self.pos = Vector(x, y)
        self.size = Vector(w, h)

        self.offset = None

    def __repr__(self):
        return "%s(%s, %s, %s, %s)" % (self.__class__.__name__, self.x, self.y, self.w, self.h)

    #===========================================================================
    # 
    #===========================================================================
    def get_x(self): return self.pos.x
    def set_x(self, val): self.pos.x = val
    x = property(get_x, set_x)

    def get_y(self): return self.pos.y
    def set_y(self, val): self.pos.y = val
    y = property(get_y, set_y)

    def get_w(self): return self.size.x
    def set_w(self, val): self.size.x = val
    w = property(get_w, set_w)

    def get_h(self): return self.size.y
    def set_h(self, val): self.size.y = val
    h = property(get_h, set_h)

    #===========================================================================
    # 
    #===========================================================================
    def get_left(self):
        if self.offset != None: return self.pos.x + self.offset.x
        return self.pos.x
    def set_left(self, val):
        if self.offset != None: self.pos.x = val - self.offset.x
        else: self.pos.x = val
    left = property(get_left, set_left)

    def get_right(self):
        if self.offset != None: return self.pos.x + self.offset.right
        return self.pos.x + self.size.x
    def set_right(self, val):
        if self.offset != None: self.pos.x = val - self.offset.right
        else: self.pos.x = val - self.size.x
    right = property(get_right, set_right)

    def get_top(self):
        if self.offset != None: return self.pos.y + self.offset.y
        return self.pos.y
    def set_top(self, val):
        if self.offset != None: self.pos.y = val - self.offset.y
        else: self.pos.y = val
    top = property(get_top, set_top)

    def get_bottom(self):
        if self.offset != None: return self.pos.y + self.offset.bottom
        return self.pos.y + self.size.y
    def set_bottom(self, val):
        if self.offset != None: self.pos.y = val - self.offset.bottom
        else: self.pos.y = val - self.size.y
    bottom = property(get_bottom, set_bottom)

    #===========================================================================
    # 
    #===========================================================================
    def get_centerX(self):
        return self.x + self.w * 0.5

    def set_centerX(self, val):
        self.x = val - self.w * 0.5

    centerX = property(get_centerX, set_centerX)


    def get_centerY(self):
        return self.y + self.h * 0.5

    def set_centerY(self, val):
        self.y = val - self.h * 0.5

    centerY = property(get_centerY, set_centerY)

    #===========================================================================
    # 
    #===========================================================================
    def set(self, *args):
        if len(args) == 1:
            self.x, self.y, self.w, self.h = args[0]
        elif len(args) == 4:
            self.x, self.y, self.w, self.h = args


    #===========================================================================
    # Collide
    #===========================================================================
    def collide(self, node):
        if isinstance(node, (list, tuple)):
            return [item for item in node if self.collide(item)]
        elif isinstance(node, Rect):
            return self.colliderect(node)



    def colliderect(self, node):
        if self.bottom < node.top: return False
        if self.top > node.bottom: return False
        if self.right < node.left: return False
        if self.left > node.right: return False
        return True
