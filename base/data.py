import os

import pygame
from pygame.locals import *



#===============================================================================
# Data
#===============================================================================
class Data(object):
    imgDict = {}
    tileSheetDict = {}

    sounds = {}

    @classmethod
    def GetPath(cls, name):
        return os.path.join("data", name)

    @classmethod
    def GetImg(cls, name):
        if not cls.imgDict.has_key(name):
            cls.imgDict[name] = pygame.image.load(cls.GetPath(name)).convert_alpha()
        return cls.imgDict[name]

#    @classmethod
#    def GetTileSheet(cls, name, width, height):
#        if not cls.tileSheetDict.has_key((name, width, height)):
#            img = cls.GetImg(name)
#            ts = []
#            imgRect = img.get_rect()
#            for h in xrange(imgRect.h / height):
#                for w in xrange(imgRect.w / width):
#                    #print "%s %s" % (w, h)
#                    ts.append(img.subsurface(w * width, h * height, width, height))
#
#            cls.tileSheetDict[name, width, height] = ts
#        return cls.tileSheetDict[name, width, height]

    @classmethod
    def GetTileSheet(cls, name, width, height):
        #from img import *
        import img
        if not cls.tileSheetDict.has_key((name, width, height)):
            cls.tileSheetDict[name, width, height] = img.TileSheet(name, width, height)
        return cls.tileSheetDict[name, width, height]

    @classmethod
    def GetSubImg(cls, name, x, y, w, h):
        return cls.GetImg(name).subsurface(x, y, w, h)

    @classmethod
    def GetSound(cls, name):
        if not cls.sounds.has_key(name):
            cls.sounds[name] = pygame.mixer.Sound(cls.GetPath(name))
        return cls.sounds[name]

    @classmethod
    def GetFont(cls, name = "visitor1.ttf"):
        return cls.GetPath(name)


