import pygame
from pygame.locals import *

from data import *
from scene import *

#===============================================================================
# Game
#===============================================================================
class Game(object):
    size = None
    keys = None
    mouse = None
    scene = None

    inst = None

    def __init__(self, size = (640, 480), background = (0, 0, 0), zoom = 1, debug = False):
        self.__class__.inst = self

        self.zoom = zoom
        self.size = size
        self.trueSize = (size[0] * zoom, size[1] * zoom)



        pygame.mixer.pre_init(frequency = 44100, channels = 1)
        pygame.init()
        #pygame.mixer.init(frequency = 44100, size = -16, channels = 2, buffer = 4096)
        pygame.mixer.pre_init(44100, -16, 2, 2048)


        self.screen = pygame.display.set_mode(self.trueSize)
        self._screen = pygame.Surface(size)

        self.clock = pygame.time.Clock()
        self.lastTick = 0

        self.quit = False

        self.background = background



        self.debug = debug

        #self.scene = None
        #self.sprites = pygame.sprite.RenderPlain()
        self.sprites = []

    #===========================================================================
    # Loop
    #===========================================================================
    def loop(self):
        while not self.quit:

            #===================================================================
            # Events
            #===================================================================
            for event in pygame.event.get(): # User did something
                if event.type == pygame.QUIT: # If user clicked close
                    self.quit = True # Flag that we are done so we exit this loop
#                elif event.type == pygame.KEYDOWN:
#                    Input.keys[event.key] = True
#                elif event.type == pygame.KEYUP:
#                    Input.keys[event.key] = False

            self.__class__.keys = pygame.key.get_pressed()
            self.__class__.mouse = pygame.mouse.get_pos()

            #===================================================================
            # Update / Render
            #===================================================================
            # Clear the screen
            #self.screen.fill((0, 0, 0))
            self._screen.fill(self.background)

#            self.sprites.update(pygame.time.get_ticks())
#            self.sprites.draw(self.screen)

            delta = (pygame.time.get_ticks() - self.lastTick) * 0.001
            for sprite in self.sprites:
                sprite.update(delta)
                sprite.draw(self._screen)

            if self.scene != None:
                self.scene.update(delta)
                self.scene.draw(self._screen)

            self.lastTick = pygame.time.get_ticks()

            if self.debug:
                font = pygame.font.Font(Data.GetFont(), 10)
                text = font.render("FPS: %i" % self.clock.get_fps(), 1, (0, 0, 0))
                textpos = text.get_rect()
                textpos.bottom = self.size[1]
                self._screen.blit(text, textpos)

            if self.zoom == 1:
                self.screen.blit(self._screen, (0, 0, self.trueSize[0], self.trueSize[1]))
            elif self.zoom == 2:
                scale = pygame.transform.scale(self._screen, self.trueSize)
                self.screen.blit(scale, (0, 0, self.trueSize[0], self.trueSize[1]))
            else:
                scale = self._screen
                for z in range(self.zoom - 1):
                    scale = pygame.transform.scale(scale, self.trueSize)
                self.screen.blit(scale, (0, 0, self.trueSize[0], self.trueSize[1]))


            self.clock.tick(60)

            pygame.display.flip()


    def add(self, sprite):
        self.sprites.append(sprite)

    @classmethod
    def GetScene(cls):
        return cls.inst.scene

    @classmethod
    def SetScene(cls, scene):
        if not isinstance(scene, Scene):
            scene = scene()

        cls.inst.scene = scene
        scene.preCreate()

    @classmethod
    def Run(cls):
        cls.inst.loop()
