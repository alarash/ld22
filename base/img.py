import pygame
from pygame.locals import *

from data import *

#===============================================================================
# TileSheet
#===============================================================================
class TileSheet(object):
    def __init__(self, fileName, width, height):
        self.fileName = fileName
        self.image = Data.GetImg(fileName)
        imgRect = self.image.get_rect()
        self.w = imgRect.w
        self.h = imgRect.h

        self.tw = width
        self.th = height

        self.images = {}

    def getImg(self, *args):
        x, y, w, h = 0, 0, 1, 1

        if len(args) == 1: #INDEX
            x = int(args[0] % (self.w / self.tw))
            y = int(args[0] / (self.w / self.tw))
        elif len(args) == 2: #X, Y
            x, y = args
        elif len(args) == 4: #X, Y, W, H
            x, y, w, h = args

#        print self.fileName
#        print x, y, w, h
#        print x * self.tw, y * self.th, w * self.tw, h * self.th

        if not self.images.has_key((x, y, w, h)):
            self.images[x, y, w, h] = self.image.subsurface(x * self.tw, y * self.th, w * self.tw, h * self.th)
        return self.images[x, y, w, h]

    def getImgs(self):
        "return a sequence of images of size (w,h)"
        ts = []
        num = int(self.h / self.th * self.w / self.tw)
        for i in xrange(num):
            ts.append(self.getImg(i))
        return ts

