import math, random
import pygame
from pygame.locals import *
from base import *

from ground import *
from mob import *

from player import *
from enemy import *

from bg import *


class PlayScene(Scene):
    inst = None
    score = 0

    #===========================================================================
    # Create
    #===========================================================================
    def create(self):
        self.__class__.inst = self
        #super(PlayScene, self).__init__()

        self.startTicks = pygame.time.get_ticks()

        #=======================================================================
        # Imgs
        #=======================================================================
        self.gameOverImg = Data.GetImg("gameover.png")
        self.sky = Data.GetImg("sky.png")
        self.pressX = Text(y = 220, txt = "Press X to Restart", fontSize = 20)
        self.pressX.centerX = Game.inst.size[0] * 0.5

        #=======================================================================
        # Nodes
        #=======================================================================
        ground = Ground()
        self.nodes.append(ground)

        self.nodes.append(Photo2(50, anim = "fan"))
        self.nodes.append(Photo(25))
        self.nodes.append(Photo3(-20))
        self.refMob = Photo2(60)
        self.nodes.append(self.refMob)

        self.player = Player(150)
        #self.nodes.append(player)
        self.nodes.append(Photo(-15))
        self.nodes.append(Photo2(35, anim = "fan"))
        self.nodes.append(Photo2(20))
        self.nodes.append(Photo2(5, anim = "fan"))

        #=======================================================================
        # 
        #=======================================================================
        self.enemies = []
        self.enemyTypes = [FanEnemy, PhotoEnemy]
        self.enemyTimer = 0

        #=======================================================================
        # Score
        #=======================================================================
        self.score = 0
        self.scoreText = Text(x = 3, y = -1, txt = "Score: 0", fontSize = 20)

        self.highScore = 0
        try:
            file = open("data/test.txt", "r")
            self.highScore = float(file.read())
            file.close()
        except:
            pass

        self.highScoreImg = Text(txt = "HighScore: %s" % self.highScore)
        self.highScoreImg.right = Game.inst.size[0]

        self.showScoreTimer = 0
        self.showScore = 0
        self.showScorePos = (0, 0)
        self.showScoreText = Text(fontSize = 30)

        self.gameOver = False
        #=======================================================================
        # Background
        #=======================================================================
        self.bgs = []
        self.bgTimer = 0.2

        self.clouds = []
        self.cloudsTimer = 0

        self.offsetY = self.player.y


    def addEnemy(self, enemyType):
        node = enemyType(500)
        self.nodes.append(node)
        self.enemies.append(node)

    def addBG(self):
        #print "BG"
        node = BG01(500)
        #self.nodes.append(node)
        self.bgs.append(node)

    def addScore(self, point, combo):
        self.score += point * combo

        self.showScoreTimer = 0.3
        self.showScore = point * combo
        self.showScorePos = (self.player.x, self.player.y)

        txt = "+%s" % self.showScore
        if combo > 1:
            txt += "(x%s)" % combo
        self.showScoreText.text = txt

    def setHighScore(self):
        if self.score > self.highScore:
            file = open("data/test.txt", "w")
            file.write("%.1f" % self.score)
            file.close()

    #===========================================================================
    # Update
    #===========================================================================
    def update(self, delta):
        super(PlayScene, self).update(delta)
        self.player.update(delta)

        #print self.enemies

        #=======================================================================
        # 
        #=======================================================================
        for node in self.enemies:
            if node.x + node.w < 0:
                self.nodes.remove(node)
                self.enemies.remove(node)

        if self.enemyTimer <= 0:
            self.enemyTimer = random.randrange(10, 100) / Ground.inst.speed * 10
            #self.enemyTimer = random.randrange(10, 100) / Ground.inst.speed * 1
            self.addEnemy(random.choice(self.enemyTypes))
        else:
            self.enemyTimer -= delta

                #self.addEnemy(node.__class__)

        #=======================================================================
        # 
        #=======================================================================
        for node in self.clouds:
            node.update(delta)
            if node.x + node.w < 0:
                self.clouds.remove(node)

        for node in self.bgs:
            node.update(delta)
            if node.x + node.w < 0:
                #self.node.remove(node)
                self.bgs.remove(node)

        if self.bgTimer <= 0:
            self.bgTimer = myRand(0.3, 3)
            self.addBG()
        else:
            self.bgTimer -= delta

        #=======================================================================
        # 
        #=======================================================================
        for node in self.clouds:
            node.update(delta)
            if node.x + node.w < 0:
                self.clouds.remove(node)

        if self.cloudsTimer <= 0:
            self.cloudsTimer = myRand(0.5, 5)
            node = Cloud(500, random.randrange(5, 40))
            self.clouds.append(node)
        else:
            self.cloudsTimer -= delta
        #=======================================================================
        # 
        #=======================================================================
        if not self.gameOver:
            self.score += delta#int((pygame.time.get_ticks() - self.startTicks) * 0.01) * 0.1
        else:
            if Game.keys[K_x]:
                #Game.inst.scene = PlayScene()
                Game.SetScene(PlayScene())


        if self.showScoreTimer != 0:
            self.showScoreTimer -= delta
            if self.showScoreTimer < 0:
                self.showScoreTimer = 0

        #=======================================================================
        # 
        #=======================================================================
        if Game.keys[K_ESCAPE]:
            Game.inst.quit = True

        objY = self.player.y
        if self.gameOver:
            objY = self.refMob.y

        self.offsetY += (objY - self.offsetY) * 5 * delta




    #===========================================================================
    # Draw
    #===========================================================================
    def draw(self, surface):
        surface.blit(self.sky, (0, 0, 320, 40))
        offset = (0, 120 - self.offsetY)

        #=======================================================================
        # Backgrounds
        #=======================================================================
        for node in self.clouds:
            node.draw(surface)
        for node in self.bgs:
            node.draw(surface, offset)

        #=======================================================================
        # Main
        #=======================================================================
        super(PlayScene, self).draw(surface, offset)
        self.player.draw(surface, offset)

        #=======================================================================
        # HUD
        #=======================================================================
        self.scoreText.text = "Score: %.1f" % self.score
        self.scoreText.draw(surface)

        if self.gameOver:
            surface.blit(self.gameOverImg, (60, 50, 201, 33))
            self.pressX.draw(surface)

        if self.showScoreTimer != 0:
            self.showScoreText.centerX = self.showScorePos[0]
            self.showScoreText.y = self.showScorePos[1] - (0.3 - self.showScoreTimer) * 30 + offset[1]
            self.showScoreText.draw(surface)

        self.highScoreImg.draw(surface)
