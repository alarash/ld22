import math, random
import pygame
from pygame.locals import *
from base import *

from playScene import *

class Ground(Node):
    inst = None

    color = (150, 150, 150)

    floorStep = 20
    speed = 50

    def __init__(self):
        super(Ground, self).__init__()
        self.__class__.inst = self

        self.floors = []
        self.floorsType = []

        self.floors.append(176)
        self.counter = 0
        for i in xrange(1 + 340 / self.floorStep):
#            t += random.randrange(-5, 5)
#            self.floors.append(t)
            self.add()

        self.offsetX = 0

        self.img = Data.GetImg("ground.png")

    #===========================================================================
    # 
    #===========================================================================
    def draw(self, surface, offset = (0, 0)):
        #pygame.draw.polygon(surface, (255, 255, 255, 255), vertices)
        #pygame.draw.rect(surface, (130, 130, 100), pygame.Rect(0, 176, 640, 480 - 176))
        #pygame.draw.line(surface, (0, 0, 0), (0, self.floorY), (320, self.floorY))
        for i, f in enumerate(self.floors):
            if i == len(self.floors) - 1: continue


#            fMax = max(f, self.floors[i + 1])
#            pygame.draw.rect(surface, self.color,
#                             (self.offsetX + i * self.floorStep, fMax,
#                              self.floorStep, 241 - fMax))

            plist = ((self.offsetX + i * self.floorStep + offset[0], f + offset[1]),
                     (self.offsetX + ((i + 1) * self.floorStep) + offset[0], self.floors[i + 1] + offset[1]),
                     (self.offsetX + ((i + 1) * self.floorStep) + offset[0], 240),
                     (self.offsetX + i * self.floorStep + offset[0], 240))
            pygame.draw.polygon(surface, self.color, plist)

            pygame.draw.line(surface, (122, 122, 122),
                             (self.offsetX + i * self.floorStep + offset[0], f + offset[1]),
                             (self.offsetX + ((i + 1) * self.floorStep) + offset[0], self.floors[i + 1] + offset[1]))

            fMax = max(f, self.floors[i + 1])
            #self.img.blit(surface, (self.offsetX + i * self.floorStep, fMax, 20, 72))
            surface.blit(self.img,
                         (self.offsetX + i * self.floorStep + offset[0], fMax + 3 + offset[1], 20, 72),
                         (20 * self.floorsType[i], 0, 20, 72))

    #===========================================================================
    # 
    #===========================================================================
    def update(self, delta):
        self.offsetX -= delta * self.speed
        if self.offsetX <= -self.floorStep: #self.floorStep:
            self.offsetX = 0
            self.floors.pop(0)
            self.floorsType.pop(0)
            self.add()

        self.speed += 3 * delta


    #===========================================================================
    # 
    #===========================================================================
    def add(self):
        prevVal = self.floors[-1]
        ratio = (prevVal / 240.0)
        #print prevVal
        valMin = int(-20.0 * ratio * 0.5)
        valMax = int(20 * (1 - ratio))


        mult = (pygame.time.get_ticks() - Game.GetScene().startTicks) * 0.000005
        #print mult

        valMin *= mult
        valMax *= mult

        #print valMin, valMax

        #val = prevVal + random.randrange(valMin, valMax)

        self.counter += myRand(valMin, valMax)#random.randrange(valMin, valMax)
        self.counter += random.randrange(-5, 5)

        self.counter *= mult
        #print self.counter
        val = prevVal + self.counter

        if val < 60:
            self.counter += myRand(0, valMax)#random.randrange(0, valMax)
            val = prevVal + self.counter
        elif val > 180:
            self.counter += myRand(valMin, 0)#random.randrange(valMin, 0)
            val = prevVal + self.counter

        val += random.randrange(-5, 5)


        self.counter *= 0.9
        self.counter += (170 - val) * 0.005

        #self.counter = val


        #test = 120 + 40 * math.sin(pygame.time.get_ticks()*0.001)
        #print PlayScene.inst.score

#        mult = self.counter * 0.01
#        test = (90 + 40 * math.sin(self.counter * 0.1 * mult) +
#                60 * math.sin(self.counter * 0.01 * mult) +
#                20 * math.cos(self.counter * 0.2 * mult))



        #val = (val * 0.8) + (test * 0.2)
        #val = test

        #if val < 50:
        #    val = 50 #+ random.randrange(-5, 5)

        self.floors.append(val)
        self.floorsType.append(random.randrange(0, 2))

    #===========================================================================
    # 
    #===========================================================================
    @classmethod
    def GetHeight(cls, x):
        return cls.inst.getHeight(x)

    def getHeight(self, x):
        x -= self.offsetX
        ind = int(x / self.floorStep)

        a = self.floors[-1]
        if ind <= len(self.floors) - 1:
            a = self.floors[ind]
        b = a
        if ind < len(self.floors) - 1:
            b = self.floors[ind + 1]


        y = a + (b - a) * (x % self.floorStep) / self.floorStep
        #return self.floors[ind]
        return y

    @classmethod
    def GetDiff(cls, x):
        return cls.inst.getDiff(x)

    def getDiff(self, x):
        Y = self.getHeight(x)
        frontY = self.getHeight(x + 1)
        return Y - frontY
