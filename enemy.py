import pygame
from pygame.locals import *
from base import *

from ground import *


#===============================================================================
# 
#===============================================================================
class Enemy(AnimSprite):
    point = 10
    speed = 50

    def __init__(self, x = 0, y = 0):
        super(Enemy, self).__init__(x, y)
        self.dead = False

    def update(self, delta):
        if not self.dead:
            self.vel.x = -Ground.inst.speed - self.speed
            self.bottom = Ground.GetHeight(self.x + self.w * 0.5)
        else:
            self.play("die")
            self.vel.y += 20

        super(Enemy, self).update(delta)

#===============================================================================
# 
#===============================================================================
class PhotoEnemy(Enemy):
    point = 20
    speed = 50

    def __init__(self, x = 0, y = 0):
        super(PhotoEnemy, self).__init__(x, y)
        self.size.set(48, 48)
        self.setImage("enemy.png")
        self.addAnim("run", (0, 1), 100)
        self.addAnim("die", (2,))

        self.offset = Rect(10, 14, 28, 34)

#===============================================================================
# 
#===============================================================================
class FanEnemy(Enemy):
    point = 10
    speed = 30

    def __init__(self, x = 0, y = 0):
        super(FanEnemy, self).__init__(x, y)
        self.size.set(24, 24)
        self.setImage("enemy.png")
        self.addAnim("run", (10, 11), 100)
        self.addAnim("die", (14,))

        self.offset = Rect(4, 4, 16, 20)
