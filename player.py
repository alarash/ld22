import pygame
from pygame.locals import *
from base import *

from ground import *

#===============================================================================
# 
#===============================================================================
class Player(AnimSprite):
    speed = 100
    def __init__(self, x = 0, y = 0):
        super(Player, self).__init__(x, y, 24, 24, img = "char.png")

        self.addAnim("run", (0, 1, 2, 3), 100)
        self.addAnim("jump", (0,))
        self.addAnim("die", (4,))

        self.jump = False
        self.bottom = Ground.inst.getHeight(self.x + 12)

        self.dead = False

        self.sndJump = Data.GetSound("jump.wav")
        self.sndHit = Data.GetSound("hit.wav")
        self.sndGameOver = Data.GetSound("gameOver.wav")

        self.combo = 0

    def update(self, delta):

        #=======================================================================
        # gameOver
        #=======================================================================
        if not Game.GetScene().gameOver:
            if self.x < 70 and not self.jump:
                self.sndGameOver.play()
                self.dead = True
                Game.GetScene().setHighScore()

            for enemy in Game.GetScene().enemies:
                #print enemy
                if not enemy.dead and self.collide(enemy):
                    if self.vel.y > 0 and self.bottom < enemy.top + 8:
                        self.sndHit.play()
                        enemy.dead = True
                        self.vel.y = -200
                        if Game.keys[K_UP]:
                            self.vel.y = -450
                        self.combo += 1
                        Game.GetScene().addScore(enemy.point, self.combo)

                    else:
                        self.sndGameOver.play()
                        self.dead = True
                        Game.GetScene().setHighScore()
                    break



        if Game.GetScene().gameOver:
            self.vel.y += 10

        if not Game.GetScene().gameOver and self.dead:
            Game.GetScene().gameOver = True
            self.play("die")
            self.vel.y = -50
            self.jump = True

        #=======================================================================
        # 
        #=======================================================================
        if not Game.GetScene().gameOver:
            self.vel.x *= 0.9
            if Game.keys[K_LEFT]: self.vel.x = -self.speed
            elif Game.keys[K_RIGHT]: self.vel.x = self.speed

            if Game.keys[K_UP] and not self.jump:
                self.sndJump.play()
                self.jump = True
                self.vel.y = -450
                self.y -= 1
                self.vel.x -= Ground.GetDiff(self.x + 12) * 400

                self.play("jump")

            if self.jump:
                #print "test"
                self.vel.y += 1600 * delta
                #self.y -= 1

                #if self.bottom > Ground.floorY:
                if self.bottom > Ground.GetHeight(self.x + 12):
                    #print "ok"
                    self.bottom = Ground.GetHeight(self.x + 12)
                    self.jump = False
                    self.vel.y = 0
                    self.play("run")

                    self.combo = 0

            else: #Follow ground
                self.bottom = Ground.GetHeight(self.x + 12)
                self.vel.x -= Ground.GetDiff(self.x + 12) * 20

            if self.x > 300:
                self.x = 300
                self.vel.x = 0


        super(Player, self).update(delta)
