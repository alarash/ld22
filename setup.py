from distutils.core import setup
import py2exe

import sys, os
sys.argv.append('py2exe')


origIsSystemDLL = py2exe.build_exe.isSystemDLL
def isSystemDLL(pathname):
       if os.path.basename(pathname).lower() in ["SDL_ttf.dll"]:
               return 0
       return origIsSystemDLL(pathname)
py2exe.build_exe.isSystemDLL = isSystemDLL


py2exe_options = dict(
                      ascii = True, # Exclude encodings
                      excludes = ['_ssl', 'pyreadline', 'difflib', 'doctest', 'locale',
                                  'optparse', 'calendar', 'pdb', 'subprocess', #'threading',
                                  'pickle',
                                  ], # Exclude standard library
                      #includes = ['sip'],
                      dll_excludes = ['msvcr71.dll', 'w9xpopen.exe'], # Exclude msvcr71

                      compressed = True, # Compress library.zip
                      bundle_files = 1,
                      optimize = 2,
                      )

setup(name = '<Name>',
      version = '1.0',
      description = '<Description>',
      author = 'Stab Alarash',

      windows = ['leaveMeAlone.py'],
      options = {'py2exe': py2exe_options},
      zipfile = None,
      )

#shutil.copy("./data", "./dist/data")
from distutils.dir_util import copy_tree
copy_tree("./data", "./dist/data")
